package com.devm8.stockalarm.dto;


import lombok.Data;

/**
 * Created by Calin on 4/6/2019.
 */
@Data
public class AlarmDTO {

    private int id;
    private String symbol;
    private double price;
    private int upperLimitPercent;
    private int lowerLimitPercent;

}
