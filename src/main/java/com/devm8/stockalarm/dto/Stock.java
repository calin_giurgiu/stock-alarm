package com.devm8.stockalarm.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.util.StringUtils;

/**
 * Created by Calin on 4/5/2019.
 */
@Data
public class Stock {

    @JsonProperty(required = true)
    @JsonAlias({"1. symbol", "01. symbol"})
    private String symbol;

    @JsonProperty(value = "2. name")
    private String name;

    private float price;

    @JsonCreator
    public Stock(@JsonProperty("05. price") String price) {
        if(!StringUtils.isEmpty(price)) {
            this.price = Float.valueOf(price);
        }
    }
}
