package com.devm8.stockalarm.repository;

import com.devm8.stockalarm.model.Alarm;
import com.devm8.stockalarm.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by Calin on 4/5/2019.
 */
public interface AlarmRepository extends PagingAndSortingRepository<Alarm, Integer> {

    List<Alarm> findBySymbolAndActiveTrue(String symbol);

    @Query("SELECT a from Alarm a where active = true and a.symbol LIKE ?1 and a.user = ?2")
    List<Alarm> findByQueryAndActiveTrue(String query, User user, Pageable pageable);

    List<Alarm> findByUserAndActiveTrue(User user);

    @Query("SELECT DISTINCT(a.symbol) from Alarm a WHERE a.active = true")
    List<String> findSymbolsDistinct();

    @Modifying
    @Query("UPDATE Alarm a SET a.active = false WHERE a.id = ?1")
    void setSymbolInactive(int id);

}
