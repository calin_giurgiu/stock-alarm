package com.devm8.stockalarm.repository;

import com.devm8.stockalarm.dto.Stock;

import java.util.List;

/**
 * Created by Calin on 4/5/2019.
 */
public interface StockRepository {

    List<Stock> searchStocks(String expression);

    Stock findStockBySymbol(String symbol);

}
