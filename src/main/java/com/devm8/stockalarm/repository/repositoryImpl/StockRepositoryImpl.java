package com.devm8.stockalarm.repository.repositoryImpl;

import com.devm8.stockalarm.dto.Stock;
import com.devm8.stockalarm.repository.StockRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Calin on 4/5/2019.
 */
@Repository
public class StockRepositoryImpl implements StockRepository {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${application.client.api-key}")
    private String apiKey;

    @Value("${application.client.stock.search-url}")
    private String searchUrl;

    @Value("${application.client.stock.find-url}")
    private String findUrl;

    @Override
    public List<Stock> searchStocks(String expression) {
        JsonNode response = restTemplate.getForObject(String.format(searchUrl, expression), JsonNode.class);
        List<Stock> stockList = new ArrayList<>();
        try {
            stockList = objectMapper.readValue(response.get("bestMatches").toString(), new TypeReference<List<Stock>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stockList;
    }

    @Override
    public Stock findStockBySymbol(String symbol) {
        JsonNode response = restTemplate.getForObject(String.format(findUrl, symbol), JsonNode.class);
        Stock result = null;
        try {
            result = objectMapper.readValue(response.get("Global Quote").toString(), Stock.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
