package com.devm8.stockalarm;

import com.devm8.stockalarm.service.AlarmService;
import com.devm8.stockalarm.util.AlarmTask;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class StockAlarmApplication {

	Timer timer;

	public static void main(String[] args) {
		SpringApplication.run(StockAlarmApplication.class, args);
		new StockAlarmApplication(new AlarmTask());
	}

	public StockAlarmApplication(AlarmTask alarmTask) {
		timer = new Timer();
		timer.schedule(alarmTask, 0, getTimeIntervalProperty() * 1000);
	}

	private int getTimeIntervalProperty() {
		PropertiesConfiguration config = new PropertiesConfiguration();
		try {
			config.load("application.properties");
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return config.getInt("application.timer.interval");
	}

	public StockAlarmApplication(){}

}
