package com.devm8.stockalarm.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;

/**
 * Created by Calin on 4/6/2019.
 */
@Configuration
public class EmailConfiguration {

    @Value("${application.mail.template}")
    private String emailTemplate;

    @Value("${application.mail.subject}")
    private String emailSubject;

    @Bean
    public SimpleMailMessage getTemplateSimpleMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setText(emailTemplate);
        message.setSubject(emailSubject);
        return message;
    }
}
