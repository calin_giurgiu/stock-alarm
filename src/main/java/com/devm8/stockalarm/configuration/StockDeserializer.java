package com.devm8.stockalarm.configuration;

import com.devm8.stockalarm.dto.Stock;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

/**
 * Created by Calin on 4/5/2019.
 */
public class StockDeserializer extends StdDeserializer<Stock> {
    protected StockDeserializer(Class<?> vc) {
        super(vc);
    }

    protected StockDeserializer(JavaType valueType) {
        super(valueType);
    }

    protected StockDeserializer(StdDeserializer<?> src) {
        super(src);
    }

    @Override
    public Stock deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return null;
    }
}
