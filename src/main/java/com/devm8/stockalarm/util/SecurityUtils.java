package com.devm8.stockalarm.util;

import com.devm8.stockalarm.model.Alarm;
import com.devm8.stockalarm.model.User;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by Calin on 4/6/2019.
 */
public class SecurityUtils {

    public static boolean isUserAuthorisedForAlarm(Alarm alarm) {
        if(alarm != null &&
                alarm.getUser().equals(SecurityContextHolder.getContext().getAuthentication().getPrincipal())) {
            return true;
        }
        return false;
    }
}
