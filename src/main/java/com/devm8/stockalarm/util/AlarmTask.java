package com.devm8.stockalarm.util;

import com.devm8.stockalarm.configuration.SpringContext;
import com.devm8.stockalarm.service.AlarmService;

import java.util.TimerTask;

/**
 * Created by Calin on 4/5/2019.
 */
public class AlarmTask extends TimerTask{

    @Override
    public void run() {
        SpringContext.getBean(AlarmService.class).processAlarms();
    }

}
