package com.devm8.stockalarm.controller;

import com.devm8.stockalarm.dto.AlarmDTO;
import com.devm8.stockalarm.error.NotFoundException;
import com.devm8.stockalarm.service.AlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by Calin on 4/6/2019.
 */
@Controller("/alarm")
public class AlarmController {

    @Autowired
    AlarmService alarmService;

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView create(AlarmDTO alarm, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("alarm-create");
        alarmService.createAlarm(alarm);

        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView alarm(AlarmDTO alarm) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("alarm-create");

        modelAndView.addObject("alarm", new AlarmDTO());
        alarmService.createAlarm(alarm);

        return modelAndView;
    }

    //Stocks for dropdown search on create alarm
    @RequestMapping(value={"/stocks"}, method = RequestMethod.GET)
    public ModelAndView stocks(String expression) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("alarm-create");

        modelAndView.addObject("stocks", alarmService.searchStocksFilteredByAlarms(expression));
        return modelAndView;
    }

    @RequestMapping(value={"/"}, method = RequestMethod.PATCH)
    public ModelAndView update(AlarmDTO alarm) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("alarm-management");
        alarmService.updateAlarm(alarm);
        modelAndView.addObject("successMessage", "Alarm successfully updated");
        return modelAndView;
    }

    @RequestMapping(value={"/"}, method = RequestMethod.DELETE)
    public ModelAndView delete(Integer id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("alarm-management");
        alarmService.deleteAlarm(id);
        modelAndView.addObject("successMessage", "Alarm successfully deleted");
        return modelAndView;
    }

    @RequestMapping(value={"/management/filter"}, method = RequestMethod.GET)
    public ModelAndView search(String query, Integer page) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("alarm-management");
        modelAndView.addObject("searchResult", alarmService.findAlarms(query, page));

        return modelAndView;
    }

    @RequestMapping(value={"/management"}, method = RequestMethod.GET)
    public ModelAndView management() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("alarm-management");

        return modelAndView;
    }
}
