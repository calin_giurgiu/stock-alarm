package com.devm8.stockalarm.error;

/**
 * Created by Calin on 4/6/2019.
 */
public class NotFoundException extends Throwable{

    public NotFoundException(String message) {
        super(message);
    }
}
