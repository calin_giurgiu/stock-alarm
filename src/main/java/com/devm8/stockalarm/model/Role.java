package com.devm8.stockalarm.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Calin on 4/4/2019.
 */
@Data
@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue
    @Column(name = "role_id")
    private int id;

    @Column(name = "role")
    private String role;
}
