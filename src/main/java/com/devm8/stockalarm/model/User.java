package com.devm8.stockalarm.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

/**
 * Created by Calin on 4/4/2019.
 */
@Data
@Entity
@Table
public class User {

    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private int id;

    @Column
    @Email(message = "Please provide an valid email",
            regexp = "[^@]+@[^\\.]+\\..+")
    @NotEmpty(message = "Please provide an email")
    private String email;

    @Column
    @Length(min = 5, message = "Your password must have at least 5")
    @NotEmpty(message = "Please provide a password")
    private String password;

    @Column(name = "first_name")
    @NotEmpty(message = "*Please provide your name")
    private String firstName;

    @Column(name = "last_name")
    @NotEmpty(message = "*Please provide your last name")
    private String lastName;

    @Column
    private int active;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
}
