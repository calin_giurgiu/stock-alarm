package com.devm8.stockalarm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

/**
 * Created by Calin on 4/5/2019.
 */
@Data
@Entity
@EqualsAndHashCode(exclude = "user")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"symbol", "user_user_id"}))
public class Alarm {

    @Id
    @GeneratedValue
    @Column(name = "alarm_id")
    private int id;

    @Column
    @NotEmpty(message = "Please provide a symbol")
    private String symbol;

    @Column
    private boolean active = true;

    @Column(precision=10, scale=2)
    private float price;

    @Min(value = 0, message = "Percentage must be positive")
    @Column(name = "upper_limit")
    private int upperLimitPercent;

    @Max(value = 0, message = "Percentage must be negative")
    @Column(name = "lower_limit")
    private int lowerLimitPercent;

    @ManyToOne(optional = false)
    private User user;

}
