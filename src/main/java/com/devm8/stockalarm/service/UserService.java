package com.devm8.stockalarm.service;

import com.devm8.stockalarm.model.Role;
import com.devm8.stockalarm.model.User;
import com.devm8.stockalarm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.Collections;

/**
 * Created by Calin on 4/4/2019.
 */
@Repository
public class UserService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role role = new Role();
        role.setRole("ADMIN");
        user.setRoles(Collections.singleton(role));
        user.setActive(1);
        userRepository.save(user);
    }

    public boolean saveUserIfUnique(User user) {
        boolean isUserUnique = userRepository.findByEmail(user.getEmail()) == null;
        if (isUserUnique) {
            saveUser(user);
        }

        return isUserUnique;
    }
}
