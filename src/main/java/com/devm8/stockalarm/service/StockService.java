package com.devm8.stockalarm.service;

import com.devm8.stockalarm.dto.Stock;
import com.devm8.stockalarm.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Calin on 4/5/2019.
 */
@Service
public class StockService {

    @Autowired
    private StockRepository stockRepository;

    public List<Stock> searchStockPrices(String expression) {
        return stockRepository.searchStocks(expression);
    }

    public Stock findStockBySymbol(String symbol) {
        return stockRepository.findStockBySymbol(symbol);
    }
}
