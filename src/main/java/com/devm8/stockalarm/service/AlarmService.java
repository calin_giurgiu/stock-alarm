package com.devm8.stockalarm.service;

import com.devm8.stockalarm.dto.AlarmDTO;
import com.devm8.stockalarm.dto.Stock;
import com.devm8.stockalarm.error.NotFoundException;
import com.devm8.stockalarm.model.Alarm;
import com.devm8.stockalarm.model.User;
import com.devm8.stockalarm.repository.AlarmRepository;
import com.devm8.stockalarm.util.SecurityUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Calin on 4/5/2019.
 */
@Service
public class AlarmService {

    @Autowired
    private StockService stockService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AlarmRepository alarmRepository;

    @Autowired
    private EmailService emailService;

    @Value("${application.pagination.items-on-page}")
    private int itemsOnPage;

    public void processAlarms() {
        List<String> savedSymbolList = alarmRepository.findSymbolsDistinct();
        if(!savedSymbolList.isEmpty()) {
            savedSymbolList.stream()
                    .forEach(s -> processActivatedAlarmsBySymbol(s));
        }
    }

    private void processActivatedAlarmsBySymbol(String symbol) {
        Stock stock = stockService.findStockBySymbol(symbol);
        List<Alarm> symbolAlarms = alarmRepository.findBySymbolAndActiveTrue(symbol);
        symbolAlarms.stream().forEach(a -> processExceededLimits(stock.getPrice(), a));
    }

    private void processExceededLimits(float currentPrice, Alarm alarm) {
        String[] params = null;
        if(isLowerLimitExceeded(currentPrice, alarm.getPrice(), alarm.getLowerLimitPercent())) {
            params = new String[] {alarm.getSymbol(), Float.toString(alarm.getPrice()),
                    Integer.toString(alarm.getLowerLimitPercent())};
        }
        if(isUpperLimitExceeded(currentPrice, alarm.getPrice(), alarm.getUpperLimitPercent())) {
            params = new String[] {alarm.getSymbol(), Float.toString(alarm.getPrice()),
                    Integer.toString(alarm.getUpperLimitPercent())};
        }
        if( params != null ) {
            emailService.sendMail(alarm.getUser().getEmail(), params);
            alarmRepository.setSymbolInactive(alarm.getId());
        }

    }

    private boolean isUpperLimitExceeded(float currentPrice, float originalPrice, int limitPercent) {
        float difference = (limitPercent * 100.0f) / originalPrice;
        if(originalPrice + difference >= currentPrice) {
            return true;
        }

        return false;
    }

    private boolean isLowerLimitExceeded(float currentPrice, float originalPrice, int limitPercent) {
        float difference = (limitPercent * 100.0f) / originalPrice;
        if(originalPrice + difference <= currentPrice) {
            return true;
        }

        return false;
    }

    public void createAlarm(AlarmDTO alarmDTO) {
        Alarm alarm = objectMapper.convertValue(alarmDTO, Alarm.class);
        alarm.setUser((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        alarmRepository.save(objectMapper.convertValue(alarmDTO, Alarm.class));
    }

    public List<Stock> searchStocksFilteredByAlarms(String expression) {
        List<Stock> stocks = stockService.searchStockPrices(expression);
        List<String> usedSymbols = alarmRepository
                .findByUserAndActiveTrue((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .stream().map(Alarm::getSymbol).collect(Collectors.toList());

        return stocks.stream()
                .filter(s -> !usedSymbols.contains(s.getSymbol()))
                .collect(Collectors.toList());
    }

    public List<AlarmDTO> findAlarms(String query, int pageNumber) {
        Pageable sortBySymbol = PageRequest.of(pageNumber, itemsOnPage, Sort.by("symbol").ascending());
        List<Alarm> alarmList = alarmRepository
                .findByQueryAndActiveTrue(String.format("\\%%s\\%", query.toUpperCase()),
                        (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal(),
                        sortBySymbol);
        List<AlarmDTO> result = new ArrayList<>();
        if(alarmList != null) {
            result = alarmList.stream()
                    .map(a ->objectMapper.convertValue(a, AlarmDTO.class))
                    .collect(Collectors.toList());
        }

        return result;
    }

    public void updateAlarm(AlarmDTO alarmDTO) throws NotFoundException {
        Alarm originalAlarm = alarmRepository.findById(alarmDTO.getId()).get();
        if(SecurityUtils.isUserAuthorisedForAlarm(originalAlarm)) {
            originalAlarm.setLowerLimitPercent(alarmDTO.getLowerLimitPercent());
            originalAlarm.setUpperLimitPercent(alarmDTO.getUpperLimitPercent());
            alarmRepository.save(originalAlarm);
        }else {
            throw new NotFoundException("Alarm not found");
        }

    }

    public void deleteAlarm(int id) throws NotFoundException {
        if(SecurityUtils.isUserAuthorisedForAlarm(alarmRepository.findById(id).get())) {
            alarmRepository.deleteById(id);
        } else {
            throw new NotFoundException("Alarm not found");
        }
    }
}
