package com.devm8.stockalarm.service;

import com.devm8.stockalarm.configuration.EmailConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * Created by Calin on 4/6/2019.
 */
@Service
public class EmailService {

    @Autowired
    private EmailConfiguration emailConfiguration;

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMail(String to, String[] params) {
        SimpleMailMessage message = emailConfiguration.getTemplateSimpleMessage();
        message.setText(String.format(message.getText(), params));
        message.setTo(to);

        javaMailSender.send(message);
    }
}
